package net.chenlin.dp.ids.server.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.UUID;

/**
 * uuid工具类
 * @author zcl<yczclcn@163.com>
 */
public class IdUtil {

    private static final Logger logger = LoggerFactory.getLogger(IdUtil.class);

    private static final String SALT_BASE = "abcdefghijklmnopqrstuvwxyz1234567890";

    private static final int SALT_LEN = 6;

    /**
     * 生成uuid，替换-，并转换大写
     * @return
     */
    public static String genId() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 生成salt
     * @return
     */
    public static String genSalt() {
        SecureRandom secureRandom = null;
        try {
            secureRandom = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            logger.error("生成密码salt异常：{}", e);
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < SALT_LEN; i++) {
            int idx = secureRandom.nextInt(SALT_BASE.length());
            sb.append(SALT_BASE.charAt(idx));
        }
        return sb.toString();
    }

    /**
     * md5加密
     * @param content
     * @return
     */
    public static String md5(String content, String salt) {
        //用于加密的字符
        char[] md5String = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'A', 'B', 'C', 'D', 'E', 'F'};
        try {
            //使用平台的默认字符集将此 String 编码为 byte序列，并将结果存储到一个新的 byte数组中
            byte[] btInput = salt.concat(content).getBytes();

            //信息摘要是安全的单向哈希函数，它接收任意大小的数据，并输出固定长度的哈希值。
            MessageDigest mdInst = MessageDigest.getInstance("MD5");

            //MessageDigest对象通过使用 update方法处理数据， 使用指定的byte数组更新摘要
            mdInst.update(btInput);

            // 摘要更新之后，通过调用digest（）执行哈希计算，获得密文
            byte[] md = mdInst.digest();

            // 把密文转换成十六进制的字符串形式
            int j = md.length;
            char[] str = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = md5String[byte0 >>> 4 & 0xf];
                str[k++] = md5String[byte0 & 0xf];
            }

            //返回经过加密后的字符串
            return new String(str);

        } catch (Exception e) {
            return null;
        }
    }

}
