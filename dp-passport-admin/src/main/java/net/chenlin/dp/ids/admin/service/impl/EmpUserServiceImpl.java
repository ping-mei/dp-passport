package net.chenlin.dp.ids.admin.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.chenlin.dp.ids.admin.dao.EmpUserMapper;
import net.chenlin.dp.ids.admin.entity.EmpUserDO;
import net.chenlin.dp.ids.admin.service.EmpUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * passport用户service
 * @author zhouchenglin[yczclcn@163.com]
 */
@Service
public class EmpUserServiceImpl implements EmpUserService {

    @Autowired
    private EmpUserMapper empUserMapper;

    /**
     * 分页查询
     * @param param
     * @return
     */
    @Override
    public Page<EmpUserDO> listPage(Map<String, Object> param) {
        Page<EmpUserDO> page = new Page<>(1, 10);
        empUserMapper.listForPage(page, param);
        return page;
    }

}
