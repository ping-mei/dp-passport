package net.chenlin.dp.ids.common.constant;

/**
 * 缓存常量
 * @author zhouchenglin[yczclcn@163.com]
 */
public class CacheConst {

    /** RSA公钥名 */
    public static final String PUBLIC_KEY = "ids:server:public:key";

    /** RSA私钥名 */
    public static final String PRIVATE_KEY = "ids:server:private:key";

}
